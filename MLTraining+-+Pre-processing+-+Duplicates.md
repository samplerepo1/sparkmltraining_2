

```python
#this notebook is for pre-processing lecture. for my own sake. 
```


```python
#survey data of citizens of Melbourne:
df = spark.createDataFrame([
        (1, 144.5, 5.9, 33, 'M'),
        (2, 167.2, 5.4, 45, 'M'),
        (3, 124.1, 5.2, 23, 'F'),
        (4, 144.5, 5.9, 33, 'M'),
        (5, 133.2, 5.7, 54, 'F'),
        (3, 124.1, 5.2, 23, 'F'),
        (5, 129.2, 5.3, 42, 'M'),
    ], ['id', 'weight', 'height', 'age', 'gender'])
```

## Getting to know your Data


```python
#counting number of rows:
df.count()
#Details:
#Count Action called to count number of rows. 
#Returns Int
```




    7




```python
#Column Names in your data-set:
df.columns
#its not callable i.e. not a method. its an attribute.
#returns a list that can be further processed.
```




    ['id', 'weight', 'height', 'age', 'gender']




```python
#Schema of your data-set:
df.printSchema()
#displays the column names, types and nullability.
```

    root
     |-- id: long (nullable = true)
     |-- weight: double (nullable = true)
     |-- height: double (nullable = true)
     |-- age: long (nullable = true)
     |-- gender: string (nullable = true)
    



```python
#Sneak Peak of your data:
df.show()
#other variants of this function:
#df.show(2) #will display two rows 
#df.show(truncate=False) #if results i.e. data in rows need not be truncated.
```

    +---+------+------+---+------+
    | id|weight|height|age|gender|
    +---+------+------+---+------+
    |  1| 144.5|   5.9| 33|     M|
    |  2| 167.2|   5.4| 45|     M|
    |  3| 124.1|   5.2| 23|     F|
    |  4| 144.5|   5.9| 33|     M|
    |  5| 133.2|   5.7| 54|     F|
    |  3| 124.1|   5.2| 23|     F|
    |  5| 129.2|   5.3| 42|     M|
    +---+------+------+---+------+
    


## Pre-Processing of Data

### Checking for Duplicates


```python
# Checking for Duplicates:
# One way: compare count() of original data-frame and dataframe with distinct
# called on it i.e. df.distinct().count()
print("Number of rows in DataFrame: {0}".format(df.count()))
print("Number of distinct rows in DataFrame: {0}".format(df.distinct().count()))

#if the counts are different, meaning there are duplicate rows in your data.
# df.distinct() returns data-frame.
```

    Number of rows in DataFrame: 7
    Number of distinct rows in DataFrame: 6



```python
#Dropping duplicate rows right-away:
df_no_duplicates = df.dropDuplicates()
df_no_duplicates.count()
print("original dataframe:")
df.show()
print("dataframe after dropping duplicate rows")
df_no_duplicates.show()
```

    original dataframe:
    +---+------+------+---+------+
    | id|weight|height|age|gender|
    +---+------+------+---+------+
    |  1| 144.5|   5.9| 33|     M|
    |  2| 167.2|   5.4| 45|     M|
    |  3| 124.1|   5.2| 23|     F|
    |  4| 144.5|   5.9| 33|     M|
    |  5| 133.2|   5.7| 54|     F|
    |  3| 124.1|   5.2| 23|     F|
    |  5| 129.2|   5.3| 42|     M|
    +---+------+------+---+------+
    
    dataframe after dropping duplicate rows
    +---+------+------+---+------+
    | id|weight|height|age|gender|
    +---+------+------+---+------+
    |  5| 133.2|   5.7| 54|     F|
    |  5| 129.2|   5.3| 42|     M|
    |  1| 144.5|   5.9| 33|     M|
    |  4| 144.5|   5.9| 33|     M|
    |  2| 167.2|   5.4| 45|     M|
    |  3| 124.1|   5.2| 23|     F|
    +---+------+------+---+------+
    



```python
df.select(["id","weight"]).show()
```

    +---+------+
    | id|weight|
    +---+------+
    |  1| 144.5|
    |  2| 167.2|
    |  3| 124.1|
    |  4| 144.5|
    |  5| 133.2|
    |  3| 124.1|
    |  5| 129.2|
    +---+------+
    



```python
[each_col for each_col in df.columns if each_col !='id']
```




    ['weight', 'height', 'age', 'gender']




```python
df.select(['weight', 'height', 'age', 'gender']).distinct().count()
```




    5




```python

df.select(['weight', 'height', 'age', 'gender']).distinct().count()
```




    5




```python
#to check for Duplicate rows in subset of columns:
df.select(
[each_col for each_col in df.columns if each_col !='id']
).distinct().count()

```




    5




```python
[c for c in df.columns if c != 'id']
```




    ['weight', 'height', 'age', 'gender']




```python
#to drop duplicate rows in sub-set of columns:
#using sub-set parameter which accepts list
df_no_dup_subset = df.dropDuplicates(subset=['weight', 'height', 'age', 'gender'])

# i.e. look for duplicate rows only in the subset of columns specified 
# and not in all of the columns.
df_no_dup_subset.count()
```




    5




```python
df_no_dup_subset.show()
```

    +---+------+------+---+------+
    | id|weight|height|age|gender|
    +---+------+------+---+------+
    |  5| 133.2|   5.7| 54|     F|
    |  1| 144.5|   5.9| 33|     M|
    |  2| 167.2|   5.4| 45|     M|
    |  3| 124.1|   5.2| 23|     F|
    |  5| 129.2|   5.3| 42|     M|
    +---+------+------+---+------+
    



```python
## if IDs are expected to be unique e.g. driver license number or SSN,
# then checking if within a column, those are unique or not:
import pyspark.sql.functions as funct
df.agg(
funct.count("id"),
funct.countDistinct("id")).show()

#can use alias for better column output:
#df.agg(
#funct.count("id"),
#funct.countDistinct("id")).show()

```

    +---------+------------------+
    |count(id)|count(DISTINCT id)|
    +---------+------------------+
    |        7|                 5|
    +---------+------------------+
    



```python
df.select("id").show()
```

    +---+
    | id|
    +---+
    |  1|
    |  2|
    |  3|
    |  4|
    |  5|
    |  3|
    |  5|
    +---+
    



```python
df_no_dup_subset.show()
```

    +---+------+------+---+------+
    | id|weight|height|age|gender|
    +---+------+------+---+------+
    |  5| 133.2|   5.7| 54|     F|
    |  1| 144.5|   5.9| 33|     M|
    |  2| 167.2|   5.4| 45|     M|
    |  3| 124.1|   5.2| 23|     F|
    |  5| 129.2|   5.3| 42|     M|
    +---+------+------+---+------+
    



```python
#if in your analysis, you want the IDs to be unique, you can generate your own
#set of unique IDs using funct.monotonically_increasing_id() method.
df.withColumn("distinctIDs",funct.monotonically_increasing_id()).show()
```

    +---+------+------+---+------+-----------+
    | id|weight|height|age|gender|distinctIDs|
    +---+------+------+---+------+-----------+
    |  1| 144.5|   5.9| 33|     M|          0|
    |  2| 167.2|   5.4| 45|     M|          1|
    |  3| 124.1|   5.2| 23|     F|          2|
    |  4| 144.5|   5.9| 33|     M|          3|
    |  5| 133.2|   5.7| 54|     F|          4|
    |  3| 124.1|   5.2| 23|     F|          5|
    |  5| 129.2|   5.3| 42|     M|          6|
    +---+------+------+---+------+-----------+
    


## Missing Values


```python
# generating a sample DataFrame:
df_miss = spark.createDataFrame([
        (1, 143.5, 5.6, 28,   'M',  100000),
        (2, 167.2, 5.4, 45,   'M',  None),
        (3, None , 5.2, None, None, None),
        (4, 144.5, 5.9, 33,   'M',  None),
        (5, 133.2, 5.7, 54,   'F',  None),
        (6, 124.1, 5.2, None, 'F',  None),
        (7, 129.2, 5.3, 42,   'M',  76000),
    ], ['id', 'weight', 'height', 'age', 'gender', 'income'])
```


```python
from pyspark.sql import Row

df_num_missing = df_miss.rdd.map(lambda each_row: \
                Row(each_row['id'],sum([c == None for c in each_row])))\
.toDF(["id","numMissing"]).sort("numMissing")

#output shows that row with ID 3 has the maximum number of missing elements.
```


```python
## to inspect further the row which has the maximum number of missing elements:
df_miss.where("id == 3").show()
```

    +---+------+------+----+------+------+
    | id|weight|height| age|gender|income|
    +---+------+------+----+------+------+
    |  3|  null|   5.2|null|  null|  null|
    +---+------+------+----+------+------+
    

