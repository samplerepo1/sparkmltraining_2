

```python
df_outliers = spark.createDataFrame([
        (1, 143.5, 5.3, 28),
        (2, 154.2, 5.5, 45),
        (3, 342.3, 5.1, 99),
        (4, 144.5, 5.5, 33),
        (5, 133.2, 5.4, 54),
        (6, 124.1, 5.1, 21),
        (7, 129.2, 5.3, 42),
    ], ['id', 'weight', 'height', 'age'])
```


```python
df_outliers.persist().count()
```




    7



Outliers are those observations that deviate significantly from the distribution of the rest of your sample.
Definition of significance varies. but generally, if all of your values lie between Q1-1.5*IQR and Q3+1.5*IQR, it means that there no outliers;
whereas IQR is defined as a difference between upper and lower quantiles.



```python
# need to find out upper and lower cut off points for each feature.
# approxQuantile method can be used. accepts three params:
#first param is the name of the column and second is a number between 0 and 1
# where 0.5 means median. third parameter is about accuracy. if you set it to 
## will calculate exact values but will be quite expensive;
```


```python
# including just the numeric columns:
cols = ["weight","height","age"]
bounds = {} #initializing an empty dictionary

for col in cols: #iterating through the list of columns defined previously
    #for a particular column in this iteration of the loop, find out quantiles
    quantiles = df_outliers.approxQuantile(
    col,[0.25,0.75],0.05) #passing a list [0.25,0.75] for first and third quantiles
    
    #finding IQR for that column:
    IQR = quantiles[1] - quantiles[0]
    #filling the dictionary with the bounds for outlier detection
    
    bounds[col] = [quantiles[0] - 1.5*IQR,
                  quantiles[1] + 1.5*IQR]

    #the result will be a dictionary
```


```python
bounds
```




    {'age': [-11.0, 93.0],
     'height': [4.499999999999999, 6.1000000000000005],
     'weight': [91.69999999999999, 191.7]}




```python
#now with such bounds in place, lets use it to flag outliers in your dataframe:
df_outliers.select(['id']+ \ #selecting id and a list of other columns
                   [
    ((df_outliers[c] < bounds[c][0]) | (df_outliers[c] > bounds[c][1])).alias(c+"_o") 
    for c in cols]).show()
#for each column value in cols, calculating this:
#df_outliers[col_name] < a value #that's how you refer to a column?
#bounds[c] indexes based on key. each column name is a key.
# bounds[c][0] indexes the first element in the value of that key.
# 
```

    +---+--------+--------+-----+
    | id|weight_o|height_o|age_o|
    +---+--------+--------+-----+
    |  1|   false|   false|false|
    |  2|   false|   false|false|
    |  3|    true|   false| true|
    |  4|   false|   false|false|
    |  5|   false|   false|false|
    |  6|   false|   false|false|
    |  7|   false|   false|false|
    +---+--------+--------+-----+
    



```python
outliers = df_outliers.select(*['id'] + [
    (
        (df_outliers[c] < bounds[c][0]) | 
        (df_outliers[c] > bounds[c][1])
    ).alias(c + '_o') for c in cols
])
outliers.show()
```

    +---+--------+--------+-----+
    | id|weight_o|height_o|age_o|
    +---+--------+--------+-----+
    |  1|   false|   false|false|
    |  2|   false|   false|false|
    |  3|    true|   false| true|
    |  4|   false|   false|false|
    |  5|   false|   false|false|
    |  6|   false|   false|false|
    |  7|   false|   false|false|
    +---+--------+--------+-----+
    



```python
df_outliers_j = df_outliers.join(outliers,on="id")
```


```python
df_outliers_j.show()
```

    +---+------+------+---+--------+--------+-----+
    | id|weight|height|age|weight_o|height_o|age_o|
    +---+------+------+---+--------+--------+-----+
    |  1| 143.5|   5.3| 28|   false|   false|false|
    |  2| 154.2|   5.5| 45|   false|   false|false|
    |  3| 342.3|   5.1| 99|    true|   false| true|
    |  4| 144.5|   5.5| 33|   false|   false|false|
    |  5| 133.2|   5.4| 54|   false|   false|false|
    |  6| 124.1|   5.1| 21|   false|   false|false|
    |  7| 129.2|   5.3| 42|   false|   false|false|
    +---+------+------+---+--------+--------+-----+
    



```python
#to filter:
df_outliers_j.filter("weight_o").select(["id","weight"]).show() #i.e. to see
#which rows have the outlier values.
# you joined two dataframes, and then filtered based on the boolean version.
# in df.filter(), you just column name. filter expects function to return
# boolean. and that column already has boolean;

```

    +---+------+
    | id|weight|
    +---+------+
    |  3| 342.3|
    +---+------+
    

